### Data model ###

Každý záznam produktu je opatřen štítky. Štítky jsou automaticky generované při úpravě produktu uživatelem s editačními právy a nastaveními aplikace (např. velikost kroku v cenovém rozsahu). Vztah mezi produkty a štítky je M:N.

Každý štítek je provázán s metadaty, které lze využít pro doplňující metadata daného štítku. Je to z důvodu optimalizace filtrování, jedná se o vztah 1:1.

Každý produkt spadá do jedné kategorie (1:N). Každá kategorie používá štítky (M:N).

![Data model](res/data_model.png)

### Modelové třídy ###

Navrhl jsem 4 třídy entit vycházející z data modelu - ProductEntity, CategoryEntity, TagEntity a TagMetadataEntity. Každá třída obsahuje list/referenci objektů dle vztahů navržených v data modelu a dále doplňující vlastnosti. Vše se mapuje do databáze skrze ORM Doctrine tak, jak je uvedeno v atributech.

### Editace produktu ###

Všechna data jsou uložená přímo u produktu (cena, výrobce, rozměry, ...), při editaci dojde v jedné transakci k aktualizaci těchto dat a k automatickému přegenerování štítků. Toto řešení lze do budoucna rozšířit o manuálně zadané štítky, kdy dopředu neznáme konkrétní potřebu štítků, nebo rozšiřovat definici produktu nedává smysl (štítek se týká pouze malé části produktů).

Příkladem štítku může být například cenový rozsah - u produktu uložíme aktuální cenu a vygenerujeme štítek s definicí cenového rozsahu, do které produkt spadá (např. od 10,- do 20,-). Každý štítek také obsahuje typ, který volíme semanticky (pro cenový rozsah např. "c_10_20" pro všechny produkty s cenou mezi 10,- a 20,-)

### Filtrace ###
Pro filtraci využijeme typ štítku. Filtr na produkty v cenovém rozsahu 10-100,- může vypadat následovně: WHERE type = "c_10_20" OR type = "c_20_30" OR (...)

### Typy štítků ###
Typ štítku udržujeme lowercase.

Cena: "c_" + startPrice + "\_" + endPrice

Výrobce: "m_" + manufacturerName

Barva "l_" + colorName

Stav "s_" + stateName

Záruka "g_" + monthsCount

Velikost "b_mm_" + sizeInMilimeters