<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

#[ORM\Entity]
#[ORM\Table(name: 'products')]
class ProductEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\Column(type: 'decimal')]
    private double $price;

    #[ORM\Column(type: 'string')]
    private string $manufacturer;

    #[ORM\Column(type: 'string')]
    private string $state;

    #[ORM\ManyToMany(targetEntity: TagEntity::class, mappedBy: 'products')]
    #[ORM\JoinTable(name: 'products_tags')]
    private Collection $tags;

    #[ORM\ManyToOne(targetEntity: CategoryEntity::class)]
    #[ORM\JoinColumn(name: 'category_id', referencedColumnName: 'id')]
    private CategoryEntity|null $category = null;

    public function __construct() {
        $this->tags = new ArrayCollection();
    }
}