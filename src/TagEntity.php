<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'tags')]
class TagEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $type;

    #[OneToOne(targetEntity: TagMetadataEntity::class)]
    #[JoinColumn(name: 'metadata_id', referencedColumnName: 'id')]
    private TagMetadataEntity|null $metadata = null;

    #[ORM\ManyToMany(targetEntity: ProductEntity::class, inversedBy: 'tags')]
    private Collection $products;

    public function __construct() {
        $this->products = new ArrayCollection();
    }
}